import { Component } from '@angular/core';
import {AguardandoPage} from "../aguardando/aguardando";
import {FeitasPage} from "../feitas/feitas";


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = AguardandoPage;
  tab2Root = FeitasPage;

  constructor() {

  }
}
