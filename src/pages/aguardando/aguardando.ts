import {Component} from '@angular/core';
import {AlertController, NavController} from 'ionic-angular';
import {NovaTarefaPage} from "../nova-tarefa/nova-tarefa";
import {CRUDTarefasService} from "../nova-tarefa/crud-tarefas.service";
import {Tarefa} from "../nova-tarefa/tarefa.model";

@Component({
  selector: 'page-aguardando',
  templateUrl: 'aguardando.html'
})
export class AguardandoPage {

  tarefas: Tarefa[];

  constructor(private navCtrl: NavController,
              private alertCtrl: AlertController,
              private tarefaService: CRUDTarefasService) {

  }

  ionViewDidEnter() {
    this.carregarTarefas();
  }

  criarNovaTarefa() {
    this.navCtrl.push(NovaTarefaPage);
  }

  editar(tarefa: Tarefa) {
    this.navCtrl.push(NovaTarefaPage, {tarefa: tarefa});
  }

  apagar(tarefa: Tarefa) {
    const confirmacao = this.alertCtrl.create({
      title: 'Apagar',
      message: 'Tem certeza que deseja apagar a tarefa "' + tarefa.nome + '"?',
      buttons: [
        {
          text: 'Não'
        },
        {
          text: 'Sim',
          handler: () => {
            this.apagarConfirmado(tarefa);
          }
        }
      ]
    });
    confirmacao.present();
  }

  private carregarTarefas() {
    this.tarefaService.readAll().subscribe(
      (res: Tarefa[]) => {
        this.tarefas = res.filter((t:Tarefa)=>t.status==='A');
      },
      (error) => console.log('erro ao recuperar tarefas -> ', error)
    );
  }

  private apagarConfirmado(tarefa: Tarefa) {
    this.tarefaService.delete(tarefa.id).subscribe(
      () => this.carregarTarefas(),
      (error) => console.log('erro ao apagar -> ', error)
    )
  }

  marcarComoFeita(tarefa: Tarefa) {
    tarefa.status='F';
    this.tarefaService.update(tarefa).subscribe(
      ()=>this.carregarTarefas(),
      (error)=>console.log('erro ao marcar como feita -> ',error)
    )
  }
}
