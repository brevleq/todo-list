import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {CRUDTarefasService} from "../nova-tarefa/crud-tarefas.service";
import {Tarefa} from "../nova-tarefa/tarefa.model";

@Component({
  selector: 'page-feitas',
  templateUrl: 'feitas.html'
})
export class FeitasPage {

  tarefas:Tarefa[];

  constructor(private navCtrl: NavController,
              private tarefaService:CRUDTarefasService) {

  }

  ionViewDidEnter(){
    this.tarefaService.readAll().subscribe(
      (res:Tarefa[])=>this.tarefas=res.filter((t:Tarefa)=>t.status==='F'),
      (error)=>console.log('erro ao recuperar tarefas feitas')
    );
  }

}
