export class Tarefa{

  id:number;
  nome:string;
  //A - Aguardando
  //F - Feita
  status: string;

  constructor(id?: number, nome?: string, status?: string) {
    this.id = id;
    this.nome = nome;
    this.status = status?status:'A';
  }
}
