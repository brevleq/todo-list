import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Tarefa} from  "./tarefa.model";
import {CRUDTarefasService} from "./crud-tarefas.service";

/**
 * Generated class for the NovaTarefaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-nova-tarefa',
  templateUrl: 'nova-tarefa.html',
})
export class NovaTarefaPage {

  tarefa:Tarefa;
  emEdicao:boolean;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public tarefaService:CRUDTarefasService) {
    this.tarefa=navParams.get('tarefa');
    if(this.tarefa)
      this.emEdicao=true;
    else {
      this.tarefa = new Tarefa();
      this.emEdicao=false;
    }
  }

  salvar(){
    if(this.emEdicao){
      this.tarefaService.update(this.tarefa).subscribe(
        () => this.navCtrl.pop(),
        (error) => console.log('erro -> ', error)
      );
    }else {
      this.tarefaService.create(this.tarefa).subscribe(
        () => this.navCtrl.pop(),
        (error) => console.log('erro -> ', error)
      );
    }
  }

}
