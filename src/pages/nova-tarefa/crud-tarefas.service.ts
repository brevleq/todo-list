import {Injectable} from "@angular/core";
import {Storage} from "@ionic/storage";
import {Tarefa} from "./tarefa.model";
import {Observable} from "rxjs";
import {observable} from "rxjs/symbol/observable";

@Injectable()
export class CRUDTarefasService {

  constructor(protected storage: Storage,) {
  }

  create(tarefa:Tarefa):Observable<any>{
    return new Observable(observer=>{
      this.readAll().subscribe(
        (tarefas:Tarefa[])=>{
          tarefa.id=this.getProximoId(tarefas);
          tarefas.push(tarefa);
          this.storage.set('tarefas', JSON.stringify(tarefas)).then(()=>{
            observer.next();
            observer.complete();
          }).catch((error)=>{
            observer.error(error);
          });
        }
      );
    });
  }

  read(id:number):Observable<Tarefa>{
    return Observable.fromPromise(this.storage.get('tarefas'))
      .flatMap(storedResult=>{
        let tarefas: Tarefa[] = JSON.parse(storedResult ? storedResult : '[]');
        let tarefa:Tarefa = tarefas.find((t:Tarefa)=>t.id===id);
        return new Observable(observer=>{
          observer.next(tarefa);
          observer.complete();
        });
      });
  }

  readAll():Observable<Tarefa[]>{
    return Observable.fromPromise(this.storage.get('tarefas'))
      .flatMap(storedResult=>{
        let tarefas: Tarefa[] = JSON.parse(storedResult ? storedResult : '[]');
        return new Observable(observer=>{
          observer.next(tarefas);
          observer.complete();
        });
      });
  }

  update(tarefa:Tarefa):Observable<any>{
    return new Observable(observer=>{
      this.readAll().subscribe(
        (tarefas:Tarefa[])=>{
          let armazenada=tarefas.find((t:Tarefa)=>t.id===tarefa.id);
          if(armazenada){
            armazenada.nome=tarefa.nome;
            armazenada.status=tarefa.status;
            this.storage.set('tarefas', JSON.stringify(tarefas)).then(()=>{
              observer.next();
              observer.complete();
            }).catch((error)=>{
              observer.error(error);
            });
          }else
            observable.error();
        }
      );
    });
  }

  delete(id):Observable<any>{
    return new Observable(observer=>{
      this.readAll().subscribe(
        (tarefas:Tarefa[])=>{
          let armazenada=tarefas.find((t:Tarefa)=>t.id===id);
          if(armazenada){
            let index = tarefas.indexOf(armazenada);
            if (index > -1)
              tarefas.splice(index, 1);
            this.storage.set('tarefas', JSON.stringify(tarefas)).then(()=>{
              observer.next();
              observer.complete();
            }).catch((error)=>{
              observer.error(error);
            });
          }else
            observer.error();
        }
      );
    });
  }

  private getProximoId(tarefas: Tarefa[]) {
    let max=0;
    for(let index:number=0;index<tarefas.length;index++){
      if(tarefas[index].id>max)
        max=tarefas[index].id;
    }
    return ++max;
  }
}
