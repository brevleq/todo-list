import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NovaTarefaPage } from './nova-tarefa';
import {IonicStorageModule} from "@ionic/storage";
import {CRUDTarefasService} from "./crud-tarefas.service";

@NgModule({
  declarations: [
    NovaTarefaPage,
  ],
  imports: [
    IonicPageModule.forChild(NovaTarefaPage),
    IonicStorageModule.forRoot()
  ],
  providers:[
    CRUDTarefasService
  ]
})
export class NovaTarefaPageModule {}
