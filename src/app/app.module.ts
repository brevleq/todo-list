import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {AguardandoPage} from "../pages/aguardando/aguardando";
import {FeitasPage} from "../pages/feitas/feitas";
import {NovaTarefaPageModule} from "../pages/nova-tarefa/nova-tarefa.module";

@NgModule({
  declarations: [
    MyApp,
    AguardandoPage,
    FeitasPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    NovaTarefaPageModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AguardandoPage,
    FeitasPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
